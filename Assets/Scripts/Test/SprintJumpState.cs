using UnityEngine;
using UnityEngine.TextCore.Text;

public class SprintJumpState : State
{
    float timePassed;
    float jumpTime;

    public SprintJumpState(PlayerController _character, StateMachine _stateMachine) : base(_character, _stateMachine)
    {
        player = _character;
        stateMachine = _stateMachine;
    }

    public override void Enter()
    {
        base.Enter();
        
        timePassed = 0f;
        

        jumpTime = 1f;
    }

    public override void Exit()
    {
        base.Exit();
        
    }

    public override void LogicUpdate()
    {

        base.LogicUpdate();
        if (timePassed > jumpTime)
        {
            
            stateMachine.ChangeState(player.sprinting);
        }
        timePassed += Time.deltaTime;
    }



}

