using UnityEngine;
using UnityEngine.TextCore.Text;

public class CrouchingState : State
{
    float playerSpeed;
    bool belowCeiling;
    bool crouchHeld;

    bool grounded;
    float gravityValue;
    Vector3 currentVelocity;


    public CrouchingState(PlayerController _character, StateMachine _stateMachine) : base(_character, _stateMachine)
    {
        player = _character;
        stateMachine = _stateMachine;
    }

    public override void Enter()
    {
        base.Enter();

        belowCeiling = false;
        crouchHeld = false;

        gravityVelocity.y = 0;

        playerSpeed = player.crouchSpeed;
        player.controller.height = player.crouchColliderHeight;
        player.controller.center = new Vector3(0f, player.crouchColliderHeight / 2f, 0f);
        grounded = player.controller.isGrounded;
        gravityValue = player.gravityValue;


    }

    public override void Exit()
    {
        base.Exit();
        player.controller.height = player.normalColliderHeight;
        player.controller.center = new Vector3(0f, player.normalColliderHeight / 2f, 0f);
        gravityVelocity.y = 0f;
        player.playerVelocity = new Vector3(input.x, 0, input.y);
        
    }

    public override void HandleInput()
    {
        base.HandleInput();
        if (crouchAction.triggered && !belowCeiling)
        {
            crouchHeld = true;
        }
        input = moveAction.ReadValue<Vector2>();
        velocity = new Vector3(input.x, 0, input.y);
        velocity = velocity.x * player.cameraTransform.right.normalized + velocity.z * player.cameraTransform.forward.normalized;
        velocity.y = 0f;
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        

        if (crouchHeld)
        {
            stateMachine.ChangeState(player.standing);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
        belowCeiling = CheckCollisionOverlap(player.transform.position + Vector3.up * player.normalColliderHeight);
        gravityVelocity.y += gravityValue * Time.deltaTime;
        grounded = player.controller.isGrounded;
        if (grounded && gravityVelocity.y < 0)
        {
            gravityVelocity.y = 0f;
        }
        currentVelocity = Vector3.Lerp(currentVelocity, velocity, player.velocityDampTime);

        player.controller.Move(currentVelocity * Time.deltaTime * playerSpeed + gravityVelocity * Time.deltaTime);

        if (velocity.magnitude > 0)
        {
            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, Quaternion.LookRotation(velocity), player.rotationDampTime);
        }
    }
    public bool CheckCollisionOverlap(Vector3 targetPositon)
    {
        int layerMask = 1 << 8;
        layerMask = ~layerMask;
        RaycastHit hit;

        Vector3 direction = targetPositon - player.transform.position;
        if (Physics.Raycast(player.transform.position, direction, out hit, player.normalColliderHeight, layerMask))
        {
            Debug.DrawRay(player.transform.position, direction * hit.distance, Color.yellow);
            return true;
        }
        else
        {
            Debug.DrawRay(player.transform.position, direction * player.normalColliderHeight, Color.white);
            return false;
        }
    }


}