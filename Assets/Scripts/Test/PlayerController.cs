using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    public CharacterController controller;
    public Vector3 playerVelocity;
    public bool groundedPlayer;
    public InputManager inputManager;

    public PlayerInput playerInput;
    public Transform cameraTransform;


    [SerializeField]
    public float gravityMultiplier = 2;
    [SerializeField]
    public float playerSpeed = 4.0f;
    [SerializeField]
    public float sprintSpeed = 10.0f;
    [SerializeField]
    public float crouchSpeed = 1.0f;
    [SerializeField]
    public float crouchColliderHeight = 1.35f;
    [SerializeField]
    public float normalColliderHeight = 1.35f;
    [SerializeField]
    public float jumpHeight = 1.0f;
    [SerializeField]
    public float gravityValue = -9.81f;

    public StateMachine movementSM;
    public StandingState standing;
    public JumpingState jumping;
    public CrouchingState crouching;
    public LandingState landing;
    public SprintState sprinting;
    public SprintJumpState sprintjumping;
    


    [Header("Animation Smoothing")]
    [Range(0, 1)]
    public float speedDampTime = 0.1f;
    [Range(0, 1)]
    public float velocityDampTime = 0.9f;
    [Range(0, 1)]
    public float rotationDampTime = 0.2f;
    [Range(0, 1)]
    public float airControl = 0.5f;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        inputManager = InputManager.Instance;
        cameraTransform = Camera.main.transform;

        movementSM = new StateMachine();
        standing = new StandingState(this, movementSM);
        jumping = new JumpingState(this, movementSM);
        crouching = new CrouchingState(this, movementSM);
        landing = new LandingState(this, movementSM);
        sprinting = new SprintState(this, movementSM);
        sprintjumping = new SprintJumpState(this, movementSM);

        movementSM.Initialize(standing);

        normalColliderHeight = controller.height;
        gravityValue *= gravityMultiplier;
    }

    void Update()
    {
       // groundedPlayer = controller.isGrounded;
       // if (groundedPlayer && playerVelocity.y < 0)
       // {
       //     playerVelocity.y = 0f;
       // }

       // Vector2 movement = inputManager.GetPlayerMovement();
       // Vector3 move = new Vector3(movement.x, 0f, movement.y);
       // move = cameraTransform.forward * move.z + cameraTransform.right * move.x;
       // move.y = 0f;
       // controller.Move(move * Time.deltaTime * playerSpeed);

       // if (move != Vector3.zero)
       // {
       //     gameObject.transform.forward = move;
       // }

       //// Changes the height position of the player..
       // if (inputManager.PlayerJumpedThisFrame() && groundedPlayer)
       // {
       //     playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
       // }

       // playerVelocity.y += gravityValue * Time.deltaTime;
       // controller.Move(playerVelocity * Time.deltaTime);

        //////////////////////////////////////////////////////////

        movementSM.currentState.HandleInput();

        movementSM.currentState.LogicUpdate();
    }

    private void FixedUpdate()
    {
        movementSM.currentState.PhysicsUpdate();
    }
}
