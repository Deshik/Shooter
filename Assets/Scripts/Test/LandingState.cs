using UnityEngine;
using UnityEngine.TextCore.Text;

public class LandingState : State
{
    float timePassed;
    float landingTime;

    public LandingState(PlayerController _character, StateMachine _stateMachine) : base(_character, _stateMachine)
    {
        player = _character;
        stateMachine = _stateMachine;
    }

    public override void Enter()
    {
        base.Enter();
        timePassed = 0f;
      
        landingTime = 0.5f;
    }

    public override void LogicUpdate()
    {

        base.LogicUpdate();
        if (timePassed > landingTime)
        {
            
            stateMachine.ChangeState(player.standing);
        }
        timePassed += Time.deltaTime;
    }



}

