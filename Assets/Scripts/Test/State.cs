using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.TextCore.Text;

public class State 
{
    public PlayerController player;
    public StateMachine stateMachine;

    protected Vector3 gravityVelocity;
    protected Vector3 velocity;
    protected Vector2 input;

    public InputAction moveAction;
    public InputAction lookAction;

    public InputAction jumpAction;

    public InputAction crouchAction;

    public InputAction sprintAction;

    public State(PlayerController _player, StateMachine _stateMachine)
    {
        player = _player;
        stateMachine = _stateMachine;
       
        moveAction = player.playerInput.actions["Move"];

        lookAction = player.playerInput.actions["Look"];
        jumpAction = player.playerInput.actions["Jump"];
        crouchAction = player.playerInput.actions["Crouch"];
        sprintAction = player.playerInput.actions["Sprint"];
        
    }

    public virtual void Enter()
    {
        //StateUI.instance.SetStateText(this.ToString());
        Debug.Log("Enter State: " + this.ToString());
    }

    public virtual void HandleInput()
    {
    }

    public virtual void LogicUpdate()
    {
    }

    public virtual void PhysicsUpdate()
    {
    }

    public virtual void Exit()
    {
    }


}
